call plug#begin('~/.local/share/nvim/plugged')

Plug 'morhetz/gruvbox'

Plug 'kien/ctrlp.vim'
Plug 'scrooloose/nerdtree'
Plug 'jistr/vim-nerdtree-tabs'
Plug 'mg979/vim-visual-multi', {'branch': 'master'}
Plug 'tpope/vim-eunuch'
Plug 'cj/vim-webdevicons'
Plug 'bling/vim-airline'
Plug 'vim-syntastic/syntastic'
Plug 'thaerkh/vim-indentguides'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'tpope/vim-surround'
Plug 'w0rp/ale'
Plug 'sheerun/vim-polyglot'
Plug 'ervandew/supertab'
Plug 'shougo/neocomplete.vim'

Plug 'xuyuanp/nerdtree-git-plugin'
Plug 'tpope/vim-fugitive'

Plug 'vim-scripts/bash-support.vim'

Plug 'nvie/vim-flake8'
Plug 'davidhalter/jedi-vim'

Plug 'gorodinskiy/vim-coloresque'
Plug 'mattn/emmet-vim'
Plug 'pangloss/vim-javascript'
Plug 'leafgarland/typescript-vim'
Plug 'peitalin/vim-jsx-typescript'
Plug 'jparise/vim-graphql'
Plug 'ternjs/tern_for_vim', { 'do' : 'npm install' }

Plug 'gabrielelana/vim-markdown'

Plug 'neoclide/coc.nvim', {'branch': 'release'}

Plug 'SirVer/ultisnips'
Plug 'honza/vim-snippets'

Plug 'artur-shaik/vim-javacomplete2'
call plug#end()

"Interface Config
syntax enable
set encoding=utf-8
set number
set relativenumber
set mouse=a

"Theme Config
set background=dark
colorscheme gruvbox

"ignore files in NERDTree
let NERDTreeIgnore=['\.pyc$', '\~$']

"Git Marks
let g:NERDTreeGitStatusIndicatorMapCustom = {
                \ 'Modified'  :'✹',
                \ 'Staged'    :'✚',
                \ 'Untracked' :'✭',
                \ 'Renamed'   :'➜',
                \ 'Unmerged'  :'═',
                \ 'Deleted'   :'✖',
                \ 'Dirty'     :'✗',
                \ 'Ignored'   :'~',
                \ 'Clean'     :'✔︎',
                \ 'Unknown'   :'?',
                \ }

" Airline
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#left_sep = ' '
let g:airline#extensions#tabline#left_alt_sep = '|'
let g:airline#extensions#tabline#formatter = 'default'

"indentguides
let g:indentguides_spacechar = '┆'
let g:indentguides_tabchar = '|'

"Emmet
let g:user_emmet_install_global = 0
autocmd FileType html,css EmmetInstall

" If you want :UltiSnipsEdit to split your window.
let g:UltiSnipsEditSplit="vertical"

" Ale
let g:ale_fix_on_save = 1
let g:ale_fixers = {
\   '*': ['remove_trailing_lines', 'trim_whitespace'],
\   'javascript': ['eslint'],
\   'python': [
\       'isort',
\       'yapf',
\       'remove_trailing_lines',
\       'trim_whitespace'
\   ]
\}

"Jedi-Vim
if isdirectory("venv")
	let g:jedi#environment_path = "venv"
	:echo "Working with your Virtual Env"
endif

"Swaping lines
function! s:swap_lines(n1, n2)
    let line1 = getline(a:n1)
    let line2 = getline(a:n2)
    call setline(a:n1, line2)
    call setline(a:n2, line1)
endfunction

function! s:swap_up()
    let n = line('.')
    if n == 1
        return
    endif

    call s:swap_lines(n, n - 1)
    exec n - 1
endfunction

function! s:swap_down()
    let n = line('.')
    if n == line('$')
        return
    endif

    call s:swap_lines(n, n + 1)
    exec n + 1
endfunction

"Shortcuts
let g:user_emmet_leader_key=',' "Emmet shortcut is "Ctrl"+","+","
nnoremap <C-o> :NERDTreeToggle <cr>
nnoremap <C-r> :NERDTreeRefreshRoot <cr>
noremap <silent> <c-s-down> :call <SID>swap_down()<CR>
nnoremap <C-s> :w! <cr>
nnoremap <C-q> :q! <cr>
noremap <silent> <c-s-up> :call <SID>swap_up()<CR>
let g:UltiSnipsExpandTrigger="<c-s-right>"
let g:UltiSnipsJumpForwardTrigger="<c-b>"
let g:UltiSnipsJumpBackwardTrigger="<c-z>"

nmap <F4> <Plug>(JavaComplete-Imports-AddSmart)
nmap <F5> <Plug>(JavaComplete-Imports-Add)
nmap <F6> <Plug>(JavaComplete-Imports-AddMissing)
nmap <F7> <Plug>(JavaComplete-Imports-RemoveUnused)
