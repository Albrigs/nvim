set -o nounset       # Treat unset variables as an error

curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
nvim +PlugInstall +qall

Coc="coc-java coc-actions coc-explorer coc-snippets coc-tsserver"

for e in $Coc; do
	nvim +CocInstall\ $e +qall
done

rm -r ~/.config/coc/extensions/coc-java-data/server/*
wget https://download.eclipse.org/jdtls/milestones/0.57.0/jdt-language-server-0.57.0-202006172108.tar.gz -P ~/.config/coc/extensions/coc-java-data/server
tar -xvf ~/.config/coc/extensions/coc-java-data/server/jdt-language-server-0.57.0-202006172108.tar.gz -C ~/.config/coc/extensions/coc-java-data/server/
rm ~/.config/coc/extensions/coc-java-data/server/jdt-language-server-0.57.0-202006172108.tar.gz
