{::options parse_block_html="true" /}
# Nvim

This repository is just my configuration folder for NEOVIM editor

## Install

__Follow these 3 steps:__
- [<svg width="14" height="14"><image xlink:href="https://www.vectorlogo.zone/logos/neovimio/neovimio-icon.svg" width="14" height="14"/></svg> Install Neovim ](https://github.com/neovim/neovim/wiki/Installing-Neovim)
- [<svg width="14" height="14"><image xlink:href="https://raw.githubusercontent.com/devicons/devicon/master/icons/git/git-original.svg" width="14" height="14"/></svg> Install Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
- Run this on your terminal:

```
git clone git@gitlab.com:Albrigs/nvim.git ~/.config/nvim; bash ~/.config/nvim/preparation.sh
```
>Just for Unix Like OS <svg width="14" height="14"><image xlink:href="https://raw.githubusercontent.com/devicons/devicon/master/icons/unix/unix-original.svg" width="14" height="14"/></svg>

## Content

### Features for:

- Bash
- Python
- JS
- React
- CSS
- HTML
- Git
- Java
- Productivity

### Shortcuts

- __Ctrl+Shift+Up__: move current line up;
- __Ctrl+Shift+Down__: move current line down;
- __Ctrl+o__: toggle nerd tree;
- __Ctrl+r__: refresh nerd tree;
- __Ctrl+s__: save file (command ":w!");
- __Ctrl+q__: exit editor (command ":q!");
- __Ctrl+,+,__: emmet;
- __Ctrl+Shift+Right__: snippets;
